# Mini-project 2, Part A: Product Adoption (due: Wed Feb 4 @ 11:59pm) #

In this assignment, you will simulate the adoption of some product within a hypothetical community. The two main conceptual models you will use are one based on an ordinary differential equation (ODE), and the other based on a simple agent-based model.

For this assignment, you will work in _assigned_ teams (pairs or groups of three).

**What to turn in**: Please "submit" a Bitbucket repo, as in the previous assignment. The repo should have your code plus a PDF file reporting your results. To submit, you need to (a) add us -- [rvuduc](https://bitbucket.org/rvuduc) and [uknowly](https://bitbucket.org/uknowly) -- as "read-only" collaborators to your repo; and (b) send an email to `cx4230-staff@lists.gatech.edu` with the URL of your repo. You only need to do one submission per team.


## Part 1: ODEs vs. Agents ##

Compare the ODE-based model of product adoption discussed in class against an agent-based model. You can write your own code, or use the code in this repo, https://bitbucket.org/gtcx4230sp15/productadoption, as a starting point.

If you use this repo, start by looking at the `demo_simODE.m` and `demo_simAgents.m` files.

The agent-based simulator we've provided allows you to vary the number of agents and the number of trial runs. Try 100, 1000, and 10000 agents; and for the number of trials, try 1, 10, 100, and 1000 trials. (That's a total of 12 combinations.) Describe what you observe. (You might include plots, or perhaps find a way to neatly summarize all of the results in one or a few plots -- use your judgement and "taste" as guides.)

> Note: When you run multiple trials, the MATLAB scripts we've provided will, for the agent-based simulation, show several curves. These correspond to the median, mean, mean + standard devaiation, and mean - standard deviation statistics on the runs.


## Part 2: Modeling network effects ##

The preceding models assume a "fully-connected" interaction network. That is, all people see all other people, and perceive the popularity of the product in the same way. A more realistic model might try to account for _network effects_, which are effects caused by each agent having its own influential neighborhood. That is, rather than each agent responding the same globally perceived popularity of the product, it only sees the popularity among its social circle.

Try running the agent-based simulator using 1000 agents, 100 trials, and a random network in which each agent has, on average, _k_ neighbors. (In the MATLAB repo, the function, `makeRandomNetwork`, will do the trick.) Try values of _k_ that include 10, 30, and 100, and summarize what you observe. In particular, how do these results compare the fully-connected case? Try to explain, in an intuitive way, why you observe what you do.

Next, try running the agent-based simulator on a _real_ social network: the [Epinions "who-trusts-whom" network][1]. There are several ways to get this network; the easiest, if you are working from within MATLAB, is to use [UFget][2] toolbox. It provides a function to download any sparse matrix in the [University of Florida's Sparse Matrix Collection][3], which contains the Epinions network, among many others.

[1]: http://snap.stanford.edu/data/soc-Epinions1.html "Epinions Who-Trusts-Whom Graph @ SNAP"
[2]: http://www.mathworks.com/matlabcentral/fileexchange/11896-ufget--matlab-interface-to-the-uf-sparse-matrix-collection "*UFget* utility for MATLAB"
[3]: http://www.cise.ufl.edu/research/sparse/matrices/ "University of Florida Sparse Matrix Collection"

Assuming you use UFget from MATLAB, here is a sample code fragment to download and build the network:

```
#!matlab

% Download
Epinions1Data = UFget('SNAP/soc-Epinions1');

% Extract the sparse matrix representing who-trusts-whom
Network = Epinions1Data.A;

% Note the number of agents in this network
numAgents = min (size (Network));

% Optional: Visualize the sparse matrix (Network)
spy (Network)

```

Simulate adoption in this network over 100 trials. Compare these results to the uniform-random model with an average of _e_ neighbors per agent, where _e_ is chosen to match the average number of neighbors per agent in the Epinions network. Describe your results, including any figures. And again, try to explain, in an intuitive way, why you observe what you do.


## Additional resources ##

The following paper gives an overview of agent-based models, and includes a short section describing the product adoption model we discussed in class. (See the subsection on _Diffusion_, p. 7285.)

http://www.pnas.org/content/99/suppl_3/7280.full

The class slides, which contain some links to resources and videos, is available on Piazza:

https://piazza.com/class_profile/get_resource/i4j35j5kp624go/i5hmt2fmv2a6g9

_UFget_ is a freely-available MATLAB-based toolbox for downloading sparse matrices from the University of Florida's Sparse Matrix Collection. For more information on how to download it or what the UF SMC is, see:

* http://www.mathworks.com/matlabcentral/fileexchange/11896-ufget--matlab-interface-to-the-uf-sparse-matrix-collection
* http://www.cise.ufl.edu/research/sparse/matrices/

[Epinions](http://epinions.com) is a website devoted to collecting user reviews of various kinds of products. More information about the sample data set may be found here: http://snap.stanford.edu/data/soc-Epinions1.html