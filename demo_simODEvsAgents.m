% =====================================
% Demo: ODE- vs. Agent-based simulation
% =====================================

% Setup main model parameters
theta = 0.4;
d = 4;
initialAdoptionsProbability = 0.05;

% ODE-specific parameters
startTime = 0;
stopTime = 150;
numTrials = 100; % for the agent-based model

% Download
Epinions1Data = UFget('SNAP/soc-Epinions1');

% Extract the sparse matrix representing who-trusts-whom
Network = Epinions1Data.A;

% Note the number of agents in this network
numAgents = min (size (Network));

% Optional: Visualize the sparse matrix (Network)
spy (Network)

% Agent-based model parameters
%numAgents = 1000;
%numTrials = 100; % for the agent-based model

% -------------------------------------------------------
% Solve ODE to compute the fraction of adopters over time
% -------------------------------------------------------
%run the uniform
numSteps = stopTime - startTime + 1;

%Network = []; % Assume fully connected
k = nnz(Network) / max(size(Network));
Network1 = makeRandomNetwork (numAgents, k); % random.  GG Dr. Vuduc

tic; % start timer
Adoptions_total1 = zeros (numSteps, numTrials);
for trial=1:numTrials,
  Adoptions = simAgents (numAgents, Network1, theta, d, ...
			 initialAdoptionsProbability, numSteps);
  Adoptions_total1(:, trial) = Adoptions;
end
fprintf ('  (Done; elapsed time: %g seconds)\n', toc);

Time_agents1 = (1:numSteps)';
Adoptions1 = Adoptions_total1 / numAgents; % normalize

% -------------------------------------------------------
% Simulate an real social network
% -------------------------------------------------------
numSteps = stopTime - startTime + 1;

%Network = []; % Assume fully connected
%k = 100;
%Network = makeRandomNetwork (numAgents, k); % random. 

tic; % start timer
Adoptions_total = zeros (numSteps, numTrials);
for trial=1:numTrials,
  Adoptions = simAgents (numAgents, Network, theta, d, ...
			 initialAdoptionsProbability, numSteps);
  Adoptions_total(:, trial) = Adoptions;
end
fprintf ('  (Done; elapsed time: %g seconds)\n', toc);

Time_agents2 = (1:numSteps)';
Adoptions2 = Adoptions_total / numAgents; % normalize

% -------------------
% Compare the results
% -------------------
plotAdoptionsODEvsAgents (Time_agents1, Adoptions1, ...
			  Time_agents2, Adoptions2);
title (sprintf ('Fraction of adopters: %d agents [%d trial(s)].', numAgents, numTrials));

% eof
